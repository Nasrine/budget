import { Router } from "express";
import { Budgetrepository } from "../repository/budgetrepository";


export const budgetcontroler=Router();


budgetcontroler.get('/all', async (req, res)=>{
    let bud= await Budgetrepository.findAll();
    res.json(bud)
})

budgetcontroler.post('/',async(req,res)=>{
    await Budgetrepository.add(req.body);
    res.json(req.body)

}) 
budgetcontroler.put('/:id',async(req,res)=>{
    await Budgetrepository.update(req.body);
    res.end()
})

//Supprimer un sujet
budgetcontroler.delete('/:id',async(req,res)=>{
    await Budgetrepository.delete(req.params.id)
    res.end()
})

budgetcontroler.get('/id/:id',async (req,res)=>{
    let bud= await Budgetrepository.findOperationById(req.params.id)
    if(!bud){
        resp.status(404).json({error: 'Not found'});
        return;
    }
    res.json(bud)
}
)
budgetcontroler.get('/month/:date', async(req, res)=>{
    let bud = await Budgetrepository.findByDate(req.params.date)

    res.json(bud)

})