import express from 'express'
import { budgetcontroler } from './controller/budgetcontroler'
import cors from "cors"


export const server = express();


server.use(express.json())
server.use(cors())

server.use('/api/budget',budgetcontroler)