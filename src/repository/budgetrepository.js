import { Budget } from "../entity/budgetEntity";
import { connection } from "./connection";

export class Budgetrepository
{
    static async findAll(){
        const [ rows ] = await connection.execute ('SELECT * FROM operations');
        const budgetEntity = []
        for (let row of  rows){
            let instance = new Budget (row.montant , row.categorie , row.date , row.id);
            budgetEntity.push(instance);
        }
        return  budgetEntity;
        }

        

static async add(operations) {
    console.log(operations)
    await connection.query ("INSERT INTO operations (montant, categorie,date) VALUE (? , ?,?)", [operations.montant,operations.categorie,operations.date,operations.id]);
}

static async update (operations) {
    const [rows] = await connection.execute('UPDATE operations SET montant=?, categorie=?, date=? WHERE id=?', [operations.montant,operations.categorie,operations.date,operations.id]);
}

static async delete (id)
{
    const [rows] = await connection.execute ('DELETE FROM operations WHERE id=?' , [id] );
} 

static async  findOperationById(id){
    const [rows] = await connection.execute('SELECT * FROM operations WHERE id=?', [id]);
    if (rows.length === 1) {
       console.log(rows)
        let instance = new Budget(rows[0].montant, rows[0].categorie, rows[0].date, rows[0].id)
console.log(instance);
        return instance;

        
    }

}
static async findByDate(date){
    const [rows] = await connection.execute('SELECT * FROM operations WHERE MONTH(date)=?', [date]);

    const operations = [];
    for(const row of rows){
        let instance = new Budget(row.montant, row.categorie, row.date, row.id);
        operations.push(instance);
    }
    return operations;
}
}  
