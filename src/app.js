import 'dotenv-flow/config'
import { server } from './server';

const port = process.env.Port || 4000;
server.listen(port,()=>{
    console.log(' listening on http://localhost:'+port)
})

// //process.on('SIGINT', () => { 
//     console.log("exiting…"); 
//     process.exit(0); 
// });

// //process.on('exit', () => { 
//     console.log("exiting…"); 
//     process.exit(0); 
// });
