DROP TABLE IF EXISTS operations;
CREATE TABLE operations(
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  montant DOUBLE NOT NULL,
  categorie VARCHAR(256),
  date DATE
);
INSERT INTO
  operations(montant, categorie, date)
VALUES
  (50, 'cinema', '2021/06/22');
INSERT INTO
  operations(montant, categorie, date)
VALUES
  (50, 'cinema', '2021-06-22');
INSERT INTO
  operations (montant, categorie, date)
VALUES
  (60, 'loyer', '2017-08-01');